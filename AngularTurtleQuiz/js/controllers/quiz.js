(function () {
    angular.module("turtleFacts")
        .controller("quizCtrl", ['$scope', 'DataService', function ($scope, DataService) {
            $scope.quizQuestions = DataService.getQuiz()
            console.log($scope.quizQuestions)
            $scope.activeQuestion = {};
            $scope.visibleQuestion = false;
            $scope.questionIndex
            $scope.submite = false;
            $scope.numberOfCorrectAnswers = 0;
            $scope.correctAnswersShow = false;
            $scope.questionShow = true;

            $scope.selectedAnswer = '';

            $scope.showQuestion = function (object, index) {
                $scope.activeQuestion = object;

                $scope.questionIndex = index;
            }

            $scope.selectedQuestion = function (answer) {

                $scope.selectedAnswer = answer;
            }



            $scope.nextQuestion = function () {
                if ($scope.selectedAnswer != '') {

                    $scope.quizQuestions[$scope.questionIndex].selected = $scope.selectedAnswer.answer;
                    $scope.selectedAnswer = '';

                }

                if ($scope.questionIndex + 1 < $scope.quizQuestions.length) {
                    var newIndex = $scope.questionIndex + 1;
                    var newObject = $scope.quizQuestions[newIndex];
                    $scope.showQuestion(newObject, newIndex)
                } else {
                    $scope.setSubmiteValue(true);
                }


            }

            $scope.setSubmiteValue = function (value) {
                $scope.submite = value;
            }

            $scope.correctAnswers = function () {
                for (i = 0; i < $scope.quizQuestions.length; i++) {
                    if ($scope.quizQuestions[i].selected == $scope.quizQuestions[i].correct) {
                        $scope.numberOfCorrectAnswers++;
                    }
                }
                $scope.correctAnswersShow = true;

                $scope.questionShow = false;
                $scope.showQuestion($scope.quizQuestions[0], 0);
            }

            $scope.previewNextQuestion = function () {
                if ($scope.questionIndex + 1 < $scope.quizQuestions.length) {
                    var newIndex = $scope.questionIndex + 1;
                    var newObject = $scope.quizQuestions[newIndex];
                    $scope.showQuestion(newObject, newIndex)
                }
            }

        }]);
})();