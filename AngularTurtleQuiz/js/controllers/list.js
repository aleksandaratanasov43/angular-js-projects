(function(){
 
    angular
        .module("turtleFacts")
        .controller("listCtrl",['$scope','DataService',function($scope,DataService){
            $scope.data = DataService.getTurtles(); 
            $scope.activeTurtle = {}; 
            $scope.search = "";
            $scope.changeActiveTurtle=function(object){
                $scope.activeTurtle = object;
            }
        }]);
 
})();
 