(function(){
 
    angular
        .module("turtleFacts")
        .controller("mainCtrl",['$scope',function($scope,DataService){
            $scope.quizActive=false;
            $scope.activateQuiz=function(){
                $scope.quizActive=true
            }
        }]);
 
})();
 